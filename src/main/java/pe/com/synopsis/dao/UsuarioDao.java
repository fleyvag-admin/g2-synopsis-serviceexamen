package pe.com.synopsis.dao;

import java.util.List;

import pe.com.synopsis.beans.Actividad;
import pe.com.synopsis.beans.Usuario;
import pe.com.synopsis.beans.response.ListaUsuarioResponse;

public interface UsuarioDao 
{
	public Usuario obtenerUsuario(int codigo);
	
	public List<Usuario> listaUsuarios();
	public Integer insertarUsuario(Usuario usuario);
	public Integer actualizarUsuario(Usuario usuario);
	public Integer eliminarUsuario(int codigo);
	public Integer actualizarActividad(Actividad actividad);
	
	
}
