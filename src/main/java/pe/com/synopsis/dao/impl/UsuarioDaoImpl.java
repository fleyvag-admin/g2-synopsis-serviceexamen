package pe.com.synopsis.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import pe.com.synopsis.beans.Actividad;
import pe.com.synopsis.beans.Usuario;
import pe.com.synopsis.dao.UsuarioDao;
@Repository
public class UsuarioDaoImpl extends JdbcDaoSupport implements UsuarioDao 
{
	private static final Logger logger = LogManager.getLogger(UsuarioDaoImpl.class);

	@Autowired
	DataSource datasource;

	@PostConstruct
	private void initialize() 
	{
		setDataSource(datasource);
	}
	@Override
	public Usuario obtenerUsuario(int codigo) 
	{
		Usuario usuario=new Usuario();
		
		String sql="SELECT * FROM participante WHERE CODIGO=?";
		try {
			usuario=getJdbcTemplate().queryForObject(sql, new Object[] {codigo},new RowMapper<Usuario>() 
			{

				@Override
				public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException 
				{
					Usuario resultado = new Usuario();
					resultado.setCodigo(rs.getInt("codigo"));
					resultado.setNombre(rs.getString("nombre"));
					resultado.setApellido(rs.getString("apellido"));
					resultado.setEdad(rs.getInt("edad"));
					resultado.setSexo(rs.getString("sexo"));
					return resultado;
					
				}
				
			});
		} catch (Exception e) 
		{
			logger.error(e);
		}
		return usuario;
	}
	@Override
	public List<Usuario> listaUsuarios() {
		String sql ="SELECT * FROM participante";
		List<Usuario> listaUsuarios = new ArrayList<Usuario>();
		
		List<Map<String,Object>> resultados = getJdbcTemplate().queryForList(sql);
		for (Map<String,Object> resultado : resultados) 
		{
			Usuario usuario = new Usuario();
			usuario.setCodigo((int) resultado.get("codigo"));
			usuario.setNombre((String)resultado.get("nombre"));
			usuario.setApellido((String)resultado.get("apellido"));
			usuario.setEdad((int)resultado.get("edad"));
			usuario.setSexo((String)resultado.get("sexo"));
			
			listaUsuarios.add(usuario);
		}
		
		return listaUsuarios;
	}
	@Override
	public Integer insertarUsuario(Usuario usuario) 
	{
		String sql="INSERT INTO participante (nombre,apellido,edad,sexo)values(?,?,?,?)";
		int respuesta=getJdbcTemplate().update(sql,new Object[] {usuario.getNombre(),usuario.getApellido(),usuario.getEdad(),usuario.getSexo()});
		return respuesta;
	}
	@Override
	public Integer actualizarUsuario(Usuario usuario) {
		String sql="UPDATE participante SET nombre=?, apellido =?, edad = ? ,sexo=? WHERE codigo = ?";
		int respuesta=getJdbcTemplate().update(sql,new Object[] {usuario.getNombre(),usuario.getApellido(),usuario.getEdad(),usuario.getSexo(),usuario.getCodigo()});
		return respuesta;
	}
	@Override
	public Integer eliminarUsuario(int codigo) {
		String sql= "DELETE  FROM participante WHERE codigo=?";
		int respuesta=getJdbcTemplate().update(sql,new Object[] {codigo});
		return respuesta;
	}
	@Override
	public Integer actualizarActividad(Actividad actividad) {
		
			String sql="UPDATE actividades SET actividad=?, codigoParticipante =?, tipo = ?  WHERE codigo = ?";
			int respuesta=getJdbcTemplate().update(sql,new Object[] {actividad.getActividad(),actividad.getCodigoParticipante(),actividad.getTipo(),actividad.getCodigo()});
			return respuesta;
	}
	
	
	
	
	
	
	
	
		
	

}
