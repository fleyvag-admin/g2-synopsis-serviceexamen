package pe.com.synopsis.beans;

public class Actividad {
	private int codigo;
	private String actividad;
	private String codigoParticipante;
	public Actividad() {
		super();
	}
	public Actividad(int codigo, String actividad, String codigoParticipante, String tipo) {
		super();
		this.codigo = codigo;
		this.actividad = actividad;
		this.codigoParticipante = codigoParticipante;
		this.tipo = tipo;
	}
	private String tipo;
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getCodigoParticipante() {
		return codigoParticipante;
	}
	public void setCodigoParticipante(String codigoParticipante) {
		this.codigoParticipante = codigoParticipante;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	

}
