package pe.com.synopsis.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.synopsis.beans.Actividad;
import pe.com.synopsis.beans.Usuario;
import pe.com.synopsis.beans.response.ListaUsuarioResponse;
import pe.com.synopsis.beans.response.UsuarioResponse;
import pe.com.synopsis.dao.UsuarioDao;
import pe.com.synopsis.service.UsuarioService;
@Service
public class UsuarioServiceImpl implements UsuarioService {
	
	private static final Logger logger = LogManager.getLogger(UsuarioServiceImpl.class);
	@Autowired
	UsuarioDao usuarioDao;

	@Override
	public UsuarioResponse obtenerDatosUsuario(int codigo) 
	{
		logger.info("Ingreso al metodo obtenerDatosUsuario()");
		UsuarioResponse response = new UsuarioResponse();
		response.setUsuario(usuarioDao.obtenerUsuario(codigo));
		return response;
	}

	@Override
	public ListaUsuarioResponse listaUsuarios() {
		logger.info("Ingreso al metodo listaUsuarios()");
		ListaUsuarioResponse response = new ListaUsuarioResponse();
		List<Usuario> ListaUsuario = usuarioDao.listaUsuarios();
		response.setListaUsuario(ListaUsuario);
		return response;
	}

	@Override
	public Integer insertarUsuario(Usuario usuario) {
		logger.info("Ingreso al metodo insertarUsuario()");

		int response=usuarioDao.insertarUsuario(usuario);
		return response;
	}

	@Override
	public Integer actualizarUsuario(Usuario usuario) {
		logger.info("Ingreso al metodo insertarUsuario()");

		int response =usuarioDao.actualizarUsuario(usuario);
		return response;
	}

	@Override
	public Integer eliminarUsuario(int codigo) {
		logger.info("Ingreso al metodo insertarUsuario()");

		int response = usuarioDao.eliminarUsuario(codigo);
		return response;
	}

	@Override
	public Integer actualizarActividad(Actividad actividad) {
		logger.info("Ingreso al metodo actualizarActividad()");

		int response =usuarioDao.actualizarActividad(actividad);
		return response;
	}
	

	


	
	

}
